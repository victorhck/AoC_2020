#!/bin/bash

lineas=$(wc -l puzzle1.txt | awk '{print $1;}')

OK=0
NOK=0

for ((i=1 ; i<=lineas ; i++))
    do
      LINEA=$(awk -v linea=$i 'FNR == linea' puzzle1.txt)
      MIN=$(echo $LINEA | cut -d '-' -f1 )
      MAX=$(echo $LINEA | sed 's/ .*//' | cut -d '-' -f2 )
      LETRA=$(echo $LINEA | awk '{print $2}' | sed 's/:.*//')
      PASS=$(echo $LINEA | awk '{print $3}')
      SOL=$(echo $PASS | grep -o $LETRA | wc -l)
      if [ $SOL -gt $MIN ] || [ $SOL -eq $MIN ] && [ $SOL -lt $MAX ] || [ $SOL -eq $MAX ];
          then 
             OK=$(($OK + 1))
          else
             NOK=$(($NOK + 1))
      fi
    done

echo "Datos OK = $OK"
echo "Datos NOK = $NOK"

