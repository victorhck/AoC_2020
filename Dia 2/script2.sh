#!/bin/bash

lineas=$(wc -l puzzle1.txt | awk '{print $1;}')

OK=0
NOK=0

for ((i=1 ; i<=lineas ; i++))
    do
      VAl=0
      NOVAL=0
      LINEA=$(awk -v linea=$i 'FNR == linea' puzzle1.txt)
      MIN=$(echo $LINEA | cut -d '-' -f1 )
      MAX=$(echo $LINEA | sed 's/ .*//' | cut -d '-' -f2 )
      LETRA=$(echo $LINEA | awk '{print $2}' | sed 's/:.*//')
      PASS=$(echo $LINEA | awk '{print $3}')
      SOL1=$( echo $PASS | awk '{print substr ($0, '$MIN', 1)}')
      SOL2=$( echo $PASS | awk '{print substr ($0, '$MAX', 1)}')

      if [ $LETRA = $SOL1 ] && [ $LETRA != $SOL2 ];
          then 
             VAL=1
          elif [ $LETRA = $SOL2 ] && [ $LETRA != $SOL1 ];
          then
             VAL=1
      else
          VAL=0
      fi
      
      
      case $VAL in
      
      0)
       
             NOK=$(($NOK + 1))
             NOVAL=0
             ;;
      
      1)     OK=$(($OK + 1))
             VAl=0
             ;;
          

      esac
      

    done

echo "Datos OK = $OK"
echo "Datos NOK = $NOK"

