# Dia 2

En el *primer reto* hay que encontrar qué *contraseñas* de las que se ofrecen en el puzzle, coinciden con la política de contraseñas.
Los primeros números indican el número minimo y máximo que puede aparecer la letra especificada en la contraseña:

```
1-3 a: ddsdaff
```

Indicará que la letra "a" deberá aparecer un mínimo de 1 vez y un máximo de 3 en la contraseña.

La respuesta será las contraseñas correctas.

---

En el *segundo reto* hay que corregir, ya que la política de contraseñas estaba equivocada.

Los números indican el caracter en la contraseña y la letra debe estar **solo** en uno de esos lugares donde indican los números.

Es decir, en el ejemplo aterior la "a" debera aparecer solo en la posición 1 o 3 de la contraseña.
