#!/bin/bash

lineas=$(wc -l puzzle.txt | awk '{print $1;}')

arbol=0
carac=1

for ((i=2 ; i<=lineas ; i++))
    do
      LINEA=$(awk -v linea=$i 'FNR == linea' puzzle.txt)
      carac=$((carac+3))
      LETRA=$(echo $LINEA | head -c $carac | tail -c 1 )
      if [ $LETRA = '#' ] ;
          then 
             arbol=$(($arbol + 1))
      fi                    
    done

echo "Datos arbol = $arbol"

