# Dia 3

En este *primer reto* de ete día. Hay que contar los *árboles* encontrados durante el descenso en tobogán desde la posición superior izquierda hasta el fondo del puzzle.
Los árboles son representados con el símbolo "#". El puzzle se repetirá en patrones añadiéndose una réplica hacia la izquierda para expandir el puzzle.

El recorrido del tobogán es de 3 posiciones hacia la derecha y una hacia abajo.

---

En el *segundo reto* continúa la misma tónica, pero ahora con otros diferentes recorridos:
* 1 a la derecha 1 abajo
* 3 a la derecha 1 abajo
* 5 a la derecha 1 abajo
* 7 a la derecha 1 abajo
* 1 a la derecha 2 abajo

Se deben calcular los árboles encontrados en cada recorrido y la solución será el producto de todos ellos.

*El puzzle está creado con Vim copiando y pegando y creando una pequeña macro para replicarlo las veces necesario*
