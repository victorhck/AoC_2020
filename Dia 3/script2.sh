#!/bin/bash


lineas=$(wc -l puzzle.txt | awk '{print $1;}')

arbol1=0
arbol2=0
arbol3=0
arbol4=0
arbol5=0

carac1=1
carac2=1
carac3=1
carac4=1
carac5=1

for ((i=2 ; i<=lineas ; i++))
    do
      LINEA=$(awk -v linea=$i 'FNR == linea' puzzle.txt)
      carac1=$((carac1+1))
      carac2=$((carac2+3))
      carac3=$((carac3+5))
      carac4=$((carac4+7))
      LETRA1=$(echo $LINEA | head -c $carac1 | tail -c 1 )
      LETRA2=$(echo $LINEA | head -c $carac2 | tail -c 1 )
      LETRA3=$(echo $LINEA | head -c $carac3 | tail -c 1 )
      LETRA4=$(echo $LINEA | head -c $carac4 | tail -c 1 )

      if [ "$LETRA1" = '#' ] ;
          then 
             arbol1=$(($arbol1 + 1))
      fi                    

      if [ "$LETRA2" = '#' ] ;
          then 
             arbol2=$(($arbol2 + 1))
      fi                    

      if [ "$LETRA3" = '#' ] ;
          then 
             arbol3=$(($arbol3 + 1))
      fi                    

      if [ "$LETRA4" = '#' ] ;
          then 
             arbol4=$(($arbol4 + 1))
      fi                    
    done

for ((i=3 ; i<=lineas ; i=i+2))
    do
      LINEA=$(awk -v linea=$i 'FNR == linea' puzzle.txt)
      carac5=$((carac5+1))
      LETRA5=$(echo $LINEA | head -c $carac5 | tail -c 1 )

      if [ "$LETRA5" = '#' ] ;
          then 
             arbol5=$(($arbol5 + 1))
      fi                    

    done
echo "Datos arboles recorrido 1 = $arbol1"
echo "Datos arboles recorrido 2 = $arbol2"
echo "Datos arboles recorrido 3 = $arbol3"
echo "Datos arboles recorrido 4 = $arbol4"
echo "Datos arboles recorrido 5 = $arbol5"
TOTAL=$(($arbol1 * $arbol2 * $arbol3 * $arbol4 * $arbol5))
echo "Total: $TOTAL"
