# Dia 1

En este **primer reto**, de la lista de números del archivo Puzzle.txt ver qué dos números suman 2020.
El producto de esos dos números será la primera respuesta.

En el **segundo reto** del primer día, ver qué tres números de la misma lista suman 2020.
El producto de esos tres números será la segunda respuesta.

*He ordenado la lista de números de manera creciente con el comando:*

```
sort -n puzzle1.txt > puzzle.txt
```
