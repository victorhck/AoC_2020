#!/bin/bash

lineas=$(wc -l puzzle.txt | awk '{print $1;}') 

for ((i=1 ; i<=lineas ; i++))
    do
    for ((j=2 ; j<=lineas ; j++))
        do
        for ((k=3 ; k<=lineas ; k++))
            do
                    DATO1=$(awk -v linea=$i 'FNR == linea' puzzle.txt)
                    DATO2=$(awk -v linea=$j 'FNR == linea' puzzle.txt)
                    DATO3=$(awk -v linea=$k 'FNR == linea' puzzle.txt)
                    SOLUCION=$(($DATO1 + $DATO2 + $DATO3))
                    if [ $SOLUCION -eq 2020 ];
                        then
                        MUL=`expr $DATO1 \* $DATO2 \* $DATO3`
                        echo "$DATO1 * $DATO2 * $DATO3 = $MUL" 
                        exit 1
                    fi
            done             
        done
    done

