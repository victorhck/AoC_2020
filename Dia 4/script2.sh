#!/bin/bash

#byr (Birth Year)
#iyr (Issue Year)
#eyr (Expiration Year)
#hgt (Height)
#hcl (Hair Color)
#ecl (Eye Color)
#pid (Passport ID)
#cid (Country ID) optional

xbyr=0
xiyr=0
xeyr=0
xhgt=0
xhcl=0
xecl=0
xpid=0
xcid=0
OK=0

lineas=$(wc -l puzzle1.txt | awk '{print $1;}')


for ((i=1 ; i<=lineas ; i++))
    do
      LINEA=$(awk -v linea=$i 'FNR == linea' puzzle1.txt)

      j=$(echo $LINEA | grep -o : | wc -l)

      for ((x=1 ; x<=j ; x++))
          do
              key=$(echo $LINEA | awk -v sep=$x '{split($0,a," "); split(a[sep],b,":");print b[1]} ')
              echo "--key= $key"
              valor=$(echo $LINEA | awk -v sep=$x '{split($0,a," "); split(a[sep],b,":");print b[2]} ')
      if [ "$key" = 'byr' ] && [ "$valor" >= '1920' ] && [ "$valor" <= '2020' ]
          then
          xbyr=1
      fi

      if [ "$key" = 'iyr' ] && [ "$valor" >= '2010' ] && [ "$valor" <= '2020' ]
          then
          xiyr=1
      fi

      if [ "$key" = 'eyr' ] && [ "$valor" >= '2020' ] && [ "$valor" <= '2030' ]
          then
          xeyr=1      
      fi

      if [ "$key" = 'hgt' ] && [ "$valor" >= '2010' ] && [ "$valor" <= '2020' ]
          then
          xhgt=1
      fi

      if [ "$key" = 'hcl' ] 
          then
          xhcl=1
      fi

      if [ "$key" = 'ecl' ] 
          then
          xecl=1
      fi
      
      if [ "$key" = 'pid' ] 
          then
          xpid=1
      fi

      if [ "$key" = 'cid' ] 
          then
          xcid=1
      fi
      done
      exit 1

      byr=$(echo $LINEA | awk 'match($0, /byr/) {print substr($0, RSTART, RLENGTH)}' )


      if [ -z "$LINEA" ] ;
      then
          pass=$((pass+1))
          if [ "$xbyr" == 1 ] && [ "$xiyr" == 1 ] && [ "$xeyr" == 1 ] && [ "$xhgt" == 1 ] && [ "$xhcl" == 1 ] && [ "$xecl" == 1 ] && [ "$xpid" == 1 ];

          then
              OK=$((OK+1))
              echo "Lin $i Pass $pass Ok $OK"
              xbyr=0
              xiyr=0
              xeyr=0
              xhgt=0
              xhcl=0
              xecl=0
              xpid=0
              xcid=0
          fi
              xbyr=0
              xiyr=0
              xeyr=0
              xhgt=0
              xhcl=0
              xecl=0
              xpid=0
              xcid=0
      fi
    done


