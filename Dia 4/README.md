# Dia 4

En el *primer reto* de este día se trata de saber cuantos pasaportes son válidos. Los pasaportes son el puzzle y consta de diferentes campos y una clave.
Cada pasaporte está separado por líneas en blanco de otros pasaportes.

Los pasaportes válidos son los que tienen los 8 campos declarados. Si no tienen el campo de CID (Country ID) pero tienen el resto también serían válidos.

*Para que el puzzle me funcionara correctamente he tenido que añadir una línea en blanco al final del puzzle.*

---


